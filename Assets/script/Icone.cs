﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Icone : MonoBehaviour
{
    public static Data data;
    public Sprite[] sprites;
    public GameObject cible;

    void Update()
    {
        if ( Data.forme == "cercle")
            cible.GetComponent<SpriteRenderer>().sprite = sprites[2];
        if (Data.forme == "carrée")
            cible.GetComponent<SpriteRenderer>().sprite = sprites[0];
        if (Data.forme == "triangle")
            cible.GetComponent<SpriteRenderer>().sprite = sprites[1];
    }
}
